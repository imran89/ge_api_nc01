﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class CheckPointController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public CheckPointController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetVisit @username, @Search ");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, int id)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetVisit_ID @id ");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Id}/{day}/{Image}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, int Id,string Image)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetVisit_Image @Image ");
                cmd.Parameters.AddWithValue("Image", Image);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Tipe}/{day}/{Long}/{Lat}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, bool Tipe, int day, float Long, float Lat,string Search, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Daily_Route_Type @username,@Tipe, @day, @long, @lat, @Search, @Company_ID ");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("tipe", Tipe);
                cmd.Parameters.AddWithValue("day", day);
                cmd.Parameters.AddWithValue("long", Long);
                cmd.Parameters.AddWithValue("lat", Lat);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }




        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{day}/{Long}/{Lat}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, int day, float Long, float Lat,Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Daily_Route @username, @day, @long, @lat, @Company_ID ");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("day", day);
                cmd.Parameters.AddWithValue("long", Long);
                cmd.Parameters.AddWithValue("lat", Lat);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }




        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{day}/{Long}/{Lat}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, int day, float Long, float Lat, string Search, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Daily_Route_Search @username, @day, @long, @lat, @Search, @Company_ID ");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("day", day);
                cmd.Parameters.AddWithValue("long", Long);
                cmd.Parameters.AddWithValue("lat", Lat);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }




        }


        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {
                string Tr_VisitJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_Visit_Json @Tr_VisitJson");
                cmd.Parameters.AddWithValue("Tr_VisitJson", Tr_VisitJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }



        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {
                string Tr_VisitJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Visit @Tr_VisitJson");
                cmd.Parameters.AddWithValue("Tr_VisitJson", Tr_VisitJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }


    }
}
