﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class FeedbackController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public FeedbackController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetFeedback @username, @Search, @Company_ID ");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id, string Search)
            {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetFeedback @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {
                string Tr_FeedbackJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_Feedback_Json @Tr_FeedbackJson");
                cmd.Parameters.AddWithValue("Tr_FeedbackJson", Tr_FeedbackJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }



        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {
                string Tr_FeedbackJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Feedback @Tr_FeedbackJson");
                cmd.Parameters.AddWithValue("Tr_FeedbackJson", Tr_FeedbackJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }


    }
}
