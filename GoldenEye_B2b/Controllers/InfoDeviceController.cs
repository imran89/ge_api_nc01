﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class InfoDeviceController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public InfoDeviceController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {
                string M_DeviceInfo_Json = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_M_DeviceInfo_Json @M_DeviceInfo_Json");
                cmd.Parameters.AddWithValue("M_DeviceInfo_Json", M_DeviceInfo_Json);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }



        }

    }
}
