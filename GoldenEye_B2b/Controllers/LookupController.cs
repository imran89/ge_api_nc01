﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class LookupController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public LookupController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik, string Search, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetProductSales @Nik,@Search,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetCategorySales @Nik,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{District}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik, string District, string Search,Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Get_Outlet @Nik, @District, @Search,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("District", District);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


    }
}

