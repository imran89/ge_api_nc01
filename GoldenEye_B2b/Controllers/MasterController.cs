﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MasterController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MasterController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Module}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, string Module, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetRespond @Search, @Module, @Company_ID ");
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Module", Module);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Module}/{Company_ID}/{Type}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, string Module, Int32 Company_ID, string Type)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetType @Search, @Module, @Company_ID ");
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Module", Module);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Search, Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_GetProductOpname @Username,@Search,@Company_ID ");
                cmd.Parameters.AddWithValue("Username", Username);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

    }
}
