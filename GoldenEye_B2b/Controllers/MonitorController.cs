﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MonitorController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MonitorController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }
        
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Company_ID}/{TransDate}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik, Int32 Company_ID, DateTime Transdate)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Monitor_TL @Nik,@Transdate,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("Transdate", Transdate);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{Nik}/{Leader_Nik}/{Company_ID}/{TransDate}")]
        public async Task Get(string Username, string Password, string dt, string keys,string Search, string Nik, string Leader_Nik, Int32 Company_ID, DateTime Transdate)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Monitor_TL_Leader @Nik,@Leader_Nik,@Transdate,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("Leader_Nik", Leader_Nik);
                cmd.Parameters.AddWithValue("Transdate", Transdate);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik,Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Monitor_TL_ALLDate @Nik,@Company_ID ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Nik}/{District}/{Search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Nik, string District, string Search,Int32 Company_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + CintaIDAPI.CLS_FUNCT.cintaidkeys + dt))
            {

                var cmd = new SqlCommand("exec SP_Get_Outlet @Nik, @District, @Search ");
                cmd.Parameters.AddWithValue("Nik", Nik);
                cmd.Parameters.AddWithValue("District", District);
                cmd.Parameters.AddWithValue("Search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


    }
}

