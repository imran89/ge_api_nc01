﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class OutletController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public OutletController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }



        // GET api/Todo
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Get_Outlet_ID @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }




        [HttpGet("{Username}/{Password}/{dt}/{keys}/{search}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, double Merchant_id, string Search,Int32 Company_ID)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Outlet_Search @Username, @search, @Company_ID");
                cmd.Parameters.AddWithValue("Username", Username);
                cmd.Parameters.AddWithValue("search", Search);
                cmd.Parameters.AddWithValue("Company_ID", Company_ID);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);
            }

        }

        // POST api/Todo
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_OutletJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Dev__Ins_M_Outlet_Json @M_OutletJson");
                cmd.Parameters.AddWithValue("M_OutletJson", M_OutletJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_ProfileJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_M_Outlet M_ProfileJson");
                cmd.Parameters.AddWithValue("M_ProfileJson", M_ProfileJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
