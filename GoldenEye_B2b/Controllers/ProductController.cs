﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace TodoApp.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public ProductController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Search}/{OutletCode}/{Company_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys,string Search, string OutletCode, Int32 Company_ID)
        {
            var cmd = new SqlCommand("EXEC SP_GetItem @outletCode, @Search, @Company_ID ");
            cmd.Parameters.AddWithValue("OutletCode", OutletCode);
            cmd.Parameters.AddWithValue("Search", Search);
            cmd.Parameters.AddWithValue("Company_ID", Company_ID);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }
    
      
    }
}
